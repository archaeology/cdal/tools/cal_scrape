# How to build a basic Shiny app

A brief introduction to creating a Shiny app in an hour using pre-existing scraping functions.

Want to see a working version instead? Go to: https://theia.arch.cam.ac.uk/events/

## Stage 0. Essential pre-requisites on your machine

- Install R - https://www.r-project.org/
- Install RStudio - https://rstudio.com/products/rstudio/download/
- Install necessary packages in your favourite way:
    - `shiny`

### Optional extras pre-requisites

- Install `Git` (you may already have it) - https://git-scm.com/book/en/v2/Getting-Started-Installing-Git
- Install a visual Git desktop tool, non-exhaustive examples below:
    - SourceTree - https://www.sourcetreeapp.com/
    - GitKraken - https://www.gitkraken.com/

## A first app: Step-by-Step instructions

### Stage A. Ducks in a row

We are going to make an interactive web-scraping app, using code already created (by Charles).

1. First let's start by getting hold of the web-scraping code. Without this, we don't really know what we need.
  - Easy way: go to the GitHub page, and download the `cbs41_cal_scrape.R` file: https://github.com/CDAL-Projects/cal_scrape
  - Advanced way: go to the GitHub page, and clone the repository/project using your favourite Git tool: `git clone https://github.com/CDAL-Projects/cal_scrape.git`
  - You can create a new folder e.g. `shinyapp` within the same folder.

2. Open up this web-scraping code within RStudio: what libraries do we need to get this thing to work?
  - Install these packages in Rstudio or however you prefer, if they are not already there.
      - `tidyverse`, `rvest`, `calendar`
  - Note the scraping script is nicely formatted into a series of re-usable functions. This will help a lot later...

3. We could start everything from scratch, but that would mean a bit of (at this point) meaningless and laborious typing. Let's start instead with a template from the Shiny demos pages.
  - Our scraping script generates a file, so we need to know how to download a file. Look here, there is a simple "file download" demo. Let's get it. https://shiny.rstudio.com/gallery/file-download.html
    - Easy way: go to the bottom of this page and cut and paste the app.R script into a new file in Rstudio.
    - Advanced way: clone all the demos from the RStudio/shiny-examples GitHub repository and pick out number 039-download-file: https://github.com/rstudio/shiny-examples
  - Save the relevant `app.R` (or twin `server.R`/`ui.R`) scripts under our new folder as `cal_scrape/shinyapp/app.R`.

4. Now we have the `app.R` open in RStudio, let's check it works. RStudio should recognise it as a Shiny app, and there should be a `Run App` button in the toolbar.
  - You'll see there are two parts to this script: the `ui` and the `server`. You sometimes see these as two separate files `ui.R` and `server.R` (plus an additional, optional, `global.R`), which makes it easier to navigate in a larger app. There's no difference in how they work. Here we're fine with a single file.

### Stage B. OK, time to meld our scripts to make something useful

#### Step 1

Because the original script organises things into functions, we can put all of these at the beginning of our script (or, in a separate file `global.R`, or in a package of its own whose functions we call etc.).

#### Step 2

Let's set up out UI inputs first; and the primary inputs are the event groupings and their URLs. We could set this up as a CSV file, but Charles' script has a nice function which identifies the categories of lecture series, which we add between the functions and the Shiny code.

```r
########## Set things up before app loads
# pull the pages, this is slow
event_groups_raw <- pull_splash()
# find the series titles/groups
event_groups <- as.vector(event_groups_raw$series_title)
message("The following seminars have been identified:-")
message(paste0(event_groups,sep="\n"))
# Turn scraped data into data frame of events with key info
events <- scrape_events(event_groups_raw)
```

If it worked, the app should list the series recognised.

#### Step 3

We want to choose which seminar series to download, so we set up an input selector using `selectInput`. Let's change the following sections of code, most importantly the `dataset` input:

```r
      # App title ----
      titlePanel("Downloading Data"),

...

      # Input: Choose dataset ----
      selectInput("dataset", "Choose a dataset:",
                  choices = c("rock", "pressure", "cars")),
```
...to...
```r
      # App title ----
      titlePanel("Archaeology Events"),

...

      # Input: Choose dataset ----
      selectInput("dataset", "Choose an event group:",
                  choices = c("",event_groups), multiple=F),
```

Try running the app now. It'll take a moment to start as it must scrape data from the homepages now. You should be able to see all the listed event groups.

#### Step 4

OK, our UI is working. Let's get the back-end to work now. First we need to change the reactive function `datasetInput`. The server can respond to changes in the `input$dataset` selector whenever this function is called. Shiny handles all of this invisibly, so you don't have to worry about HTTP.

```r
# Reactive value for selected dataset ----
  datasetInput <- reactive({
    switch(input$dataset,
           "rock" = rock,
           "pressure" = pressure,
           "cars" = cars)
  })
```
...should change to something like...
```r
datasetInput <- reactive({
    eventgroup <- input$dataset
    return(events %>% filter(X1 %in% eventgroup))
  })
```

What is this doing? It is checking for the value of input$dataset (which is the selector we created), and then filtering the pre-populated events already scraped based on the `X1` column.

If you run the app, you should see that the app now outputs the contents of `events` as a table. If the original script had more meaningful colnames, these would be displayed instead of `X1`, `X2`. (We'd also have to change the filter above). The key bit here is the `output$table` function, which you may have noticed was also referenced in the `ui` above as `tableOutput("table")`. We could rename this something more specific to clarify the purpose in our app, e.g. `events_table`, we have to do this in both the UI and the server sections: `tableOutput("table")` and `output$events_table`.

#### Step 5

If you click on the Download button, at the moment it just downloads the table as a CSV file. Useful, but not what we actually want. We want an iCal file. So let's change the file download section.
```r
# Downloadable csv of selected dataset ----
  output$downloadData <- downloadHandler(
    filename = function() {
      paste(input$dataset, ".csv", sep = "")
    },
    content = function(file) {
      write.csv(datasetInput(), file, row.names = FALSE)
    }
  )
```
...to...
```r
# Downloadable ical file of selected events ----
  output$downloadData <- downloadHandler(
    filename = function() {
      # export as ical, with a filename based on the event group
      paste0(input$dataset, ".ics")
    },
    content = function(file) {
      # convert events to ical format
      selection <- datasetInput()
      ical <- format_vevents(selection)
      # Use ic_write from calender package
      ic_write(ical, file)
    }
  )
```
Here, the `downloadHandler()` function is linked to the `output$downloadData` button that was there in the UI already. All we have done is change the filename it output withs and the

Try it out and see if it worked! Your own working Shiny app for personal use!

### Stage C. Further Fun with Shiny

So why stop here? Since Shiny is entirely based in R, we can do some fancy visualisation and other fun on top.

#### C.1 Essential: Dark Theme

Shiny uses the Bootstrap CSS and Javascript library to manage its HTML UI. Usefully there are a bunch of themes available via the `shinythemes` package: https://rstudio.github.io/shinythemes/

So let's get a nice dark theme ready, install the package:
```r
install.packages("shinythemes")
```
Then add to our app.R script (or ui.R script) in the right places:
```r
library(shinythemes)

...

# Define UI for data download app ----
ui <- fluidPage(theme=shinytheme("superhero"),
```

**Phew! Now, finally, Erik will be happy!**

#### C.2 Nice: Plots, e.g. day of week of events

The `events` data.frame encodes a lot of interesting data we could use. Affiliation of speaker, days and times of events. All of which we can analyse and visualise. Let's create a simple plot to see which days of the week are busiest.

We'll need to add in a ui element to the ui section, for simplicity let's just put it underneath the tableOutput even if that isn't the best place:

```r
# Main panel for displaying outputs ----
    mainPanel(

      tableOutput("events_table"),# Watch out for correct use of commas in the Shiny UI code, they can make the whole thing break down easily

      plotOutput("events_barplot")

    )
```
Then in the server section, we need to add some R code to create the plot:-
```r
# Plot events by days of the week ----
 output$events_barplot <- renderPlot({
   # if you want this to be specific to the selected group, you can change events to datasetInput*()
   selection <- events
   days_of_week <- weekdays(as.POSIXct(ic_datetime(selection$X4)))
   order_days <- c("Monday",
              "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday")
   days_of_week <- factor(days_of_week, levels= rev(order_days))
   dow_freq <- table(days_of_week)

   par(mar=c(3,6,0,1))
   barplot(dow_freq,horiz=T, las=2, col="darkorange", col.axis="grey50")
 }, bg="transparent")
```

Run the app and see... Isn't `Tuesday` a busy day at the moment?!

If you're familiar with R, you'll see how easy it is to make a plot using normal code. You can do the same thing with `ggplot2` if you like ggplot visuals better:

```r
# Plot events by days of the week using ggplot2 ----
  output$events_ggplot_barplot <- renderPlot({
    require("ggplot2")
    # if you want this to be specific to the selected group, you can change events to datasetInput*()
    selection <- events
    days_of_week <- weekdays(as.POSIXct(ic_datetime(selection$X4)))
    order_days <- c("Monday",
                    "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday")
    days_of_week <- factor(days_of_week, levels= rev(order_days))

    dow <- data.frame(day=days_of_week) # ggplot needs a data.frame
    ggplot(dow) + geom_bar(aes(x=day)) + coord_flip()
  }, bg="transparent")
```

Just change the plotOutput to "events_ggplot_barplot", if you want to keep both types there. Change the definition of `selection` to `datasetInput()` to make it use the current event group. (Though this might be kind of boring as most event groups have their events on the same day).

#### C.3 Other things you could try yourself...

- **EASY**: Change the `datasetInput` reactive code so that when no group is selected, ALL events are shown instead of none.
- **EASY**: Rewrite `scrape_events()` and `format_vevents()` so that the column titles are more meaningful when displayed on the table. (Or cheaters's way would be to use dplyr::rename on datasetInput() within the `output$events_table` function.)
- **EASY**: Change the `selectInput` settings so that multiple event groups can be selected.
- **EASY**: Make the ggplot version of the barchart into a polar barchart using `coord_polar()`.

- **MEDIUM**: Put the actual scraping function into the `datasetInput` reactive (rather than pre-loading all on app load)
- **MEDIUM**: Rewrite the `format_vevents()` function and add some selectors in the UI so that users to choose how the iCal events appear in the calendar (e.g. a shorter *Garrod: Cameron Petrie* instead of *Cameron Petrie (University of Cambridge) @ Garrod Research Seminars*)...
- **MEDIUM**: Add handling of *Other Cambridge Events* to `scrape_events()`, so that these appear in their own Group categories, not all lumped together. (You'll also have to move and change the line `event_groups <- as.vector(event_groups_raw$series_title)`).
- **MEDIUM**: Attempt to classify the talks, using keywords in the title, attempting to guess gender of speaker, and visualise these through time
- **MEDIUM**: Make app crash more gracefully when it fails to connect to/scrape from departmental website, using `try()` or `tryCatch()`.
- **MEDIUM**: Create action button to launch function which re-downloads web-scraped data when user chooses to (instead of just on app load)

- **ADVANCED**: Cache the web-scraping from `pull_splash()` in an external file and only update once a day when loaded so that the app loads faster for most users.
- **ADVANCED**: Create a calendar heatmap visualising the number of events per day: see http://www.columbia.edu/~sg3637/blog/Time_Series_Heatmaps.html , https://dominikkoch.github.io/Calendar-Heatmap/ or using `timevis`
- **ADVANCED**: Create a visualisation which shows the affiliation of the speakers spatially, using `Leaflet` or `tmap`.

- **SUPER GEEK**: Write the scraping procedure in `Python` then use R package `reticulate` to import scraping results into the Shiny app before load.

## General advice for planning web apps

Ideally you want to plan your app in advance and ask yourself a series of questions
- what do you want it to do?
- who is your audience?
- can you make it as simple as possible?
- where will it be hosted? (what server? who pays?)
- who will maintain it into the future?

## Further reading and learning

- Browse the Shiny Gallery and Demos: https://shiny.rstudio.com/gallery/

- DON'T CHEAT: but if you wanted to, you could also download the source code for the working version of the https://theia.arch.cam.ac.uk/events/ webapp from: https://gitlab.com/cdal/cal_scrape/-/tree/shiny-app/shinyapp
